Feature: Create and Edit feature

  Scenario: As a user create a fill up info
    When I see the text "Fillup information"
    Then I enter text "2" into field with id "price"
    Then take picture
    Then I enter text "15" into field with id "volume"
    Then take picture
    Then I enter text "15000" into field with id "odometer"
    Then take picture
    Then I press "Save Fillup"
    And I wait

Scenario: As a user create other a fill up info
    When I see the text "Fillup information"
    Then I enter text "7" into field with id "price"
    Then take picture
    Then I enter text "20" into field with id "volume"
    Then take picture
    Then I enter text "35000" into field with id "odometer"
    Then take picture
    Then I press "Save Fillup"
    And I wait    

  Scenario: As a user edit a fill up info
    When I see the text "History"
    Then I press "History"
    Then I press "15.00 g"
    Then I press "Edit"
    Then I clear input field with id "price"
    Then I enter text "9" into field with id "price"
    Then take picture
    Then I clear input field with id "volume"
    Then I enter text "15" into field with id "volume"
    Then take picture
    Then I press "Tank was not filled to the top"
    Then take picture
    Then I press "Save changes"
    Then I see the text "Statistics"
    And I wait

  Scenario: As a user see statistics
    When I see the text "Statistics"
    Then I press "Statistics"
    When I see the text "Fillup costs"
    Then I see "$275.00"
    And I wait